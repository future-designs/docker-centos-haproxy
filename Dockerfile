FROM centos:latest

MAINTAINER Jacob Lohse <j.lohse@traso.de>

ENV container docker

# install epel and webtatic repos
RUN rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm \
&& rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

# update
RUN yum -y update

# copy all configs and vendor to docker /tmp

RUN mkdir -p /opt/mydocker/config \
 && mkdir -p /opt/mydocker/vendor

COPY config /opt/mydocker/config/
COPY vendor /opt/mydocker/vendor/

# install haproxy / haproxy configuration
RUN yum -y install haproxy

# clear all
RUN yum clean all

# portforwarding
EXPOSE 80

CMD ["/usr/sbin/init"]
