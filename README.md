# docker-centos-haproxy

Docker with CentOS 7, Apache 2, PHP 7.1

# Docker Installation
```bash
yum install docker
```
# Docker Configuration

autostart/first start:
```bash
systemctl enable docker

systemctl start docker
```
# Docker-Compose Installation

```bash
curl -L "https://github.com/docker/compose/releases/download/1.8.1/docker-compose-$(uname -s)-$(uname -m)" > /usr/bin/docker-compose
chmod +x /usr/bin/docker-compose
```
(check for latest Version, and paste !)

# Building Container

Run as ROOT !

```bash
docker-compose up -d --build
```
# Docker Bash

```bash
docker-compose exec DOCKERNAME /bin/bash
```
# HaProxy